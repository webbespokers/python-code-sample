import os, json, stripe
from django.db import models
from django.utils import timezone
from datetime import datetime
from fracturedveil.packages.models import Package
from django_resized import ResizedImageField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now
from django.utils import timezone
from fracturedveil.website_api.model import GenericClient


def upload_avatar_to(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return 'players/%s%s' % (
        now().strftime("%Y%m%d%H%M%S"),
        filename_ext.lower(),
    )


class PlayerImage(models.Model):

    name = models.CharField(max_length=75, blank=True)
    avatar = ResizedImageField(
        size=[500, 500],
        crop=['middle', 'center'],
        upload_to='players',
        blank=True)
    player_name = models.CharField(max_length=75, blank=True)
    created = models.DateTimeField(default=datetime.now, blank=True)
    modified = models.DateTimeField(default=datetime.now, blank=True)


DEFAULT_PACKAGE_ID = 6


class Player(models.Model):

    player_name = models.CharField(max_length=75, blank=True)
    email = models.CharField(blank=True, max_length=75)
    google_id = models.CharField(max_length=75, blank=True)
    is_production = models.BooleanField(default=False)
    package = models.ForeignKey(
        Package, default=DEFAULT_PACKAGE_ID, related_name="package_set")
    biography = models.TextField(blank=True)
    real_name = models.CharField(max_length=75, blank=True)
    birthday = models.CharField(max_length=75, blank=True)
    twitch = models.CharField(max_length=75, blank=True)
    facebook = models.CharField(max_length=75, blank=True)
    twitter = models.CharField(max_length=75, blank=True)
    steam = models.CharField(max_length=75, blank=True)
    amazon = models.CharField(max_length=75, blank=True)
    stripe_customer_id = models.CharField(max_length=75, blank=True)
    auth_user_id = models.IntegerField(blank=True, default=0)

    profile_completed = models.DateTimeField(blank=True, null=True)
    facebook_synced = models.DateTimeField(blank=True, null=True)
    twitter_synced = models.DateTimeField(blank=True, null=True)
    google_synced = models.DateTimeField(blank=True, null=True)
    twitch_synced = models.DateTimeField(blank=True, null=True)
    steam_synced = models.DateTimeField(blank=True, null=True)

    avatar = ResizedImageField(
        size=[500, 500],
        crop=['middle', 'center'],
        upload_to='players',
        blank=True)

    location_timezone = models.CharField(max_length=75, blank=True)
    privacy_show_in_search = models.CharField(
        max_length=3, default='No', choices=[('No', 'No'), ('Yes', 'Yes')])
    privacy_primary_email = models.CharField(
        max_length=75,
        blank=True,
        default='only_me',
        choices=[('only_me', 'Only Me'), ('only_friends', 'Only Friends'),
                 ('everyone', 'Everyone')])
    privacy_real_name = models.CharField(
        max_length=75,
        blank=True,
        default='only_me',
        choices=[('only_me', 'Only Me'), ('only_friends', 'Only Friends'),
                 ('everyone', 'Everyone')])
    privacy_birthday = models.CharField(
        max_length=75,
        blank=True,
        default='only_me',
        choices=[('only_me', 'Only Me'), ('only_friends', 'Only Friends'),
                 ('everyone', 'Everyone')])
    privacy_profile = models.CharField(
        max_length=75,
        blank=True,
        default='public',
        choices=[('public', 'Show profile to public'),
                 ('friends', 'Show profile to friends'),
                 ('disable', 'Disable public profile')])
    url_hash = models.CharField(max_length=90, blank=True)
    referrer_name = models.CharField(max_length=75, blank=True)
    number_of_referrals = models.IntegerField(blank=True, default=0)

    create_update_debug = models.TextField(blank=True)

    created = models.DateTimeField(default=datetime.now, blank=True)
    modified = models.DateTimeField(default=datetime.now, blank=True)

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        return super(Player, self).save(*args, **kwargs)

    def __str__(self):
        return self.player_name


def save_url_hash(pk, url_hash):
    from django.db import connection
    cursor = connection.cursor()
    cursor.execute("UPDATE players_player SET url_hash = %s WHERE id = %s",
                   [url_hash, pk])


class PlayerContact(models.Model):

    player = models.ForeignKey(
        Player, related_name="contacts", null=True, blank=True)
    name = models.CharField(max_length=75, blank=True)
    contact_id = models.CharField(max_length=75, blank=True)
    contact_type = models.CharField(max_length=75, blank=True)
    deleted = models.BooleanField(default=0)


@receiver(
    post_save, sender=PlayerContact, dispatch_uid="player_contact_checks")
def player_contact_checks(sender, instance, **kwargs):

    print('TEST', instance.player.player_name)

    return


class PlayerFriendInvite(models.Model):

    player = models.ForeignKey(
        Player, related_name="friend_invites", null=True, blank=True)
    is_production = models.BooleanField(default=False)
    player_name = models.CharField(max_length=75, blank=True)
    friend_name = models.CharField(max_length=75, blank=True)
    friend_email = models.CharField(max_length=75, blank=True)
    friend_message = models.TextField(blank=True)
    friend_status = models.CharField(
        max_length=75,
        blank=True,
        default='Pending',
        choices=[('Pending', 'Pending'), ('Accepted', 'Accepted'),
                 ('Rejected', 'Rejected')])
    created = models.DateTimeField(default=datetime.now, blank=True)
    modified = models.DateTimeField(default=datetime.now, blank=True)

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        return super(PlayerFriendInvite, self).save(*args, **kwargs)

    def __str__(self):
        return self.player_name

    def __unicode__(self):
        return unicode(self.player_name) or u''


class PlayerPayment(models.Model):

    player = models.ForeignKey(
        Player, related_name="payment", null=True, blank=True)
    description = models.CharField(max_length=75, blank=True)
    stripe_response = models.TextField(blank=True)
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    created = models.DateTimeField(default=datetime.now, blank=True)
    modified = models.DateTimeField(default=datetime.now, blank=True)

    def save(self, *args, **kwargs):
        self.modified = timezone.now()
        return super(PlayerPayment, self).save(*args, **kwargs)

    def __str__(self):
        return self.stripe_response


def post_payment_process(sender, instance, **kwargs):

    payment = PlayerPayment.objects.get(pk=instance.pk)
    player = Player.objects.get(pk=payment.player_id)
    stripe_response = json.loads(payment.stripe_response)

    data = {
        'package_name': player.package.title,
        'amount_charged_cents': stripe_response['amount'],
        'stripe_transaction_id': stripe_response['id'],
    }

    client = GenericClient(user_access_token)
    resp = client.notify_package_purchased(data)

    if (resp['status'] != 'success'):

        if (settings.IS_PRODUCTION):
            stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
        else:
            stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

        refund = stripe.Refund.create(charge=stripe_response['id'])
        player_refund = PlayerPayment(
            stripe_response=refund,
            player_id=player.pk,
            amount=stripe_response['amount'],
        )
        player_refund.save()

    return
