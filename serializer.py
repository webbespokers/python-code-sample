from rest_framework import serializers
from models import Player


class PlayerSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    package_image_url = serializers.SerializerMethodField()

    class Meta:
        model = Player
        depth = 1
        fields = (
            'id',
            'player_name',
            'email',
            'real_name',
            'package',
            'birthday',
            'biography',
            'twitch',
            'facebook',
            'twitter',
            'amazon',
            'steam',
            'avatar',
            'package_image_url',
            'location_timezone',
            'privacy_show_in_search',
            'privacy_show_in_search',
            'privacy_primary_email',
            'privacy_real_name',
            'privacy_birthday',
            'privacy_profile',
            'url_hash',
            'referrer_name',
            'number_of_referrals',
        )

    def get_avatar(self, obj):
        if obj.avatar.name:
            return 'https://storage.googleapis.com/frvwebstaging-gcs-bucket/' \
                + obj.avatar.name
        else:
            return ''

    def get_package_image_url(self, obj):
        if obj.package.image.name:
            return 'https://storage.googleapis.com/frvwebstaging-gcs-bucket/' \
                + obj.package.image.name
        else:
            return ''
