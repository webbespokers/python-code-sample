For a futuristic multiplayer desktop game of survival, we’ve created a web application making it possible to i.a. buy goods that can be used in the game, manage player’s profile, find other players and interact with them by using chat functionality.

The code sample presents Python code responsible for Players management.
