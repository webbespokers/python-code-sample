from django.contrib import admin
from fracturedveil.players.models import Player, PlayerPayment, PlayerFriendInvite


class PlayerAdmin(admin.ModelAdmin):
    list_display = ["player_name", "email", "package"]
    list_filter = ["player_name", "email", "package"]

    fields = [
        "player_name",
        "email",
        "avatar",
        "package",
        "real_name",
        "birthday",
        "twitch",
        "facebook",
        "twitter",
        "steam",
        "amazon",
        "biography",
        "location_timezone",
        "privacy_show_in_search",
        "privacy_primary_email",
        "privacy_real_name",
        "privacy_profile",
        "referrer_name",
        "create_update_debug",
        "profile_completed",
    ]


class PlayerPaymentAdmin(admin.ModelAdmin):
    list_display = ["player", "description", "created"]
    list_filter = ["player", "description", "created"]

    fields = [
        "player",
        "description",
        "amount",
        "stripe_response",
    ]


class PlayerFriendInviteAdmin(admin.ModelAdmin):
    list_display = ["player_name", "friend_email", "friend_status"]
    list_filter = ["player_name", "friend_email", "friend_status"]

    fields = [
        "player_name",
        "friend_name",
        "friend_email",
        "friend_message",
        "friend_status",
    ]


admin.site.register(Player, PlayerAdmin)
admin.site.register(PlayerPayment, PlayerPaymentAdmin)
admin.site.register(PlayerFriendInvite, PlayerFriendInviteAdmin)
