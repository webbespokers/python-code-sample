from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from fracturedveil.players import views
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^save_contacts/$', views.SavePlayerContacts.as_view()),
    url(r'^friend_invite/$', views.SavePlayerFriendInvite.as_view()),
    url(r'^get_friend_invites/$', views.GetPlayerFriendInvites.as_view()),
    url(r'^cancel_friend_invite/$', views.CancelFriendInvite.as_view()),
    url(r'^reject_friend_invite/$', views.RejectFriendInvite.as_view()),
    url(r'^get_contacts/$', views.GetPlayerContacts.as_view()),
    url(r'^get_referrals/$', views.GetPlayerReferrals.as_view()),
    url(r'^set_contact_as_deleted/$', views.SetContactAsDeleted.as_view()),
    url(r'^get_by_name/$', views.GetPlayerByName.as_view()),
    url(r'^payment/$', views.ProcessPayment.as_view()),
    url(r'^payment_history/$', views.GetPlayerPayments.as_view()),
    url(r'^read_player_data/$', views.ReadPlayerData.as_view()),
    url(r'^update_player_privacy/$', views.UpdatePlayerPrivacy.as_view()),
    url(r'^update_player_timezone/$', views.UpdatePlayerTimezone.as_view()),
    url(r'^update_player_social/$', views.UpdatePlayerSocial.as_view()),
    url(r'^update_player_birthday/$', views.UpdatePlayerBirthday.as_view()),
    url(r'^update_player_biography/$', views.UpdatePlayerBiography.as_view()),
    url(r'^update_player_avatar/$', views.UpdatePlayerAvatar.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
