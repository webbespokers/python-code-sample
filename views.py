import os, json
from django.http import HttpResponse
from rest_framework import permissions, status, generics
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Player, PlayerContact, PlayerPayment, Package, PlayerFriendInvite, PlayerImage
from fracturedveil.players.serializer import PlayerSerializer
from django.forms.models import model_to_dict
from django.conf import settings
from fracturedveil import utils
from fracturedveil.website_api.model import GenericClient
from fracturedveil.mail_api import mail_helper
import json, stripe, time, logging
from fracturedveil.website_api.model import GenericClient
from django.utils import timezone


class UpdatePlayerPrivacy(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])
        rpc_data = {
            'show_in_search_results':
            utils.str2bool(data['show_in_search_results']),
            'show_email_to_friends':
            utils.str2bool(data['show_email_to_friends']),
            'show_email_to_everyone':
            utils.str2bool(data['show_email_to_everyone']),
            'show_realname_to_friends':
            utils.str2bool(data['show_realname_to_friends']),
            'show_realname_to_everyone':
            utils.str2bool(data['show_realname_to_everyone']),
        }
        resp = client.update_player_privacy(rpc_data)
        privacy_response = resp['response']
        if (resp['status_code'] == status.HTTP_200_OK):
            return Response({
                'error_message': privacy_response.error_message
            },
                            status=status.HTTP_200_OK)
        else:
            return Response({
                'error_message':
                'There was a problem updating player privacy.'
            },
                            status=resp['status_code'])


class UpdatePlayerTimezone(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])
        rpc_data = {'utc_offset': int(data['utc_offset'])}
        resp = client.update_player_timezone(rpc_data)
        timezone_response = resp['response']
        if (resp['status_code'] == status.HTTP_200_OK):
            return Response({
                'error_message': timezone_response.error_message
            },
                            status=status.HTTP_200_OK)
        else:
            return Response({
                'error_message':
                'There was a problem updating timezone settings'
            },
                            status=resp['status_code'])


class UpdatePlayerSocial(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])

        if ('twitch' in data):
            rpc_data = {'twitch': data['twitch']}
            resp = client.update_player_twitch(rpc_data)
        if ('twitter' in data):
            rpc_data = {'twitter': data['twitter']}
            resp = client.update_player_twitter(rpc_data)
        if ('facebook' in data):
            rpc_data = {'facebook': data['facebook']}
            resp = client.update_player_facebook(rpc_data)
        if ('steam' in data):
            rpc_data = {'steam': data['steam']}
            resp = client.update_player_steam(rpc_data)
        if ('amazon' in data):
            rpc_data = {'amazon': data['amazon']}
            resp = client.update_player_amazon(rpc_data)

        return Response({'error_message': ''}, status=status.HTTP_200_OK)


class UpdatePlayerBirthday(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])
        rpc_data = {
            'birth_month': int(data['birth_month']),
            'birth_day': int(data['birth_day']),
            'birth_year': int(data['birth_year']),
        }
        resp = client.update_player_birthday(rpc_data)
        birthday_response = resp['response']
        if (resp['status_code'] == status.HTTP_200_OK):
            return Response({
                'error_message': birthday_response.error_message
            },
                            status=status.HTTP_200_OK)
        else:
            return Response({
                'error_message':
                'There was a problem updating birthday settings'
            },
                            status=resp['status_code'])


class UpdatePlayerBiography(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])
        rpc_data = {'biography': data['biography']}
        resp = client.update_player_biography(rpc_data)
        print('UPDATE BIOGRAPHY', resp)
        biography_response = resp['response']
        if (resp['status_code'] == status.HTTP_200_OK):
            return Response({
                'error_message': biography_response.error_message
            },
                            status=status.HTTP_200_OK)
        else:
            return Response({
                'error_message':
                'There was a problem updating biography settings'
            },
                            status=resp['status_code'])


class UpdatePlayerAvatar(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )
        data = request.data

        if ('avatar' in data):
            extension = os.path.splitext(data['avatar_name'])[1]
            filename = 'player-avatar-' + str(timezone.now()) + extension
            image_file = utils.image_from_binary(data['avatar'])
            player_image = PlayerImage.objects.create(name=filename)
            player_image.avatar.save(filename, image_file)
            data[
                'avatar'] = 'https://storage.googleapis.com/frvwebstaging-gcs-bucket/' + player_image.avatar.name

            print('AVATAR', data)

            rpc_data = {'avatar': data['avatar']}
            client = GenericClient(data['oauth_token'])
            resp = client.update_player_avatar(rpc_data)

            if (resp['status_code'] == status.HTTP_200_OK):
                return Response(rpc_data, status=status.HTTP_200_OK)

        return Response({
            'error_message': 'There was a problem uploading avatar'
        },
                        status=status.HTTP_200_OK)


class ReadPlayerData(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )

        data = request.data
        client = GenericClient(data['oauth_token'])
        player_response = client.read_player_data({})

        if (player_response['status_code'] == status.HTTP_200_OK):
            player_data = player_response['response']
            print('READ PLAYER DATA', player_data)
            player = {
                'error_message':
                player_data.error_message,
                'email':
                player_data.email,
                'player_name':
                player_data.player_name,
                'birth_date':
                player_data.birth_date,
                'utc_offset':
                player_data.utc_offset,
                'referrer_name':
                player_data.referrer_name,
                'package_purchased':
                player_data.package_purchased,
                'playable':
                player_data.playable,
                'avatar':
                player_data.avatar,
                'biography':
                player_data.biography,
                'show_in_search_results':
                player_data.show_in_search_results,
                'show_email_to_friends':
                player_data.show_email_to_friends,
                'show_email_to_everyone':
                player_data.show_email_to_everyone,
                'show_realname_to_friends':
                player_data.show_realname_to_friends,
                'show_realname_to_everyone':
                player_data.show_realname_to_everyone,
                'twitch':
                player_data.twitch,
                'facebook':
                player_data.facebook,
                'twitter':
                player_data.twitter,
                'steam':
                player_data.steam,
                'amazon':
                player_data.amazon,
                'house_name':
                player_data.house_name,
                'house_tag':
                player_data.house_tag,
                'house_role':
                player_data.house_role,
            }

            if (player_data.house_name):
                rpc_data = {'house_name': player_data.house_name}
                house_branding_response = client.read_house_branding(rpc_data)
                if (house_branding_response['status_code'] == status.
                        HTTP_200_OK):
                    house_branding = house_branding_response['response']
                    player['house_logo'] = house_branding.house_logo
                    player[
                        'house_background_image'] = house_branding.house_background_image
                    player['house_color'] = house_branding.house_color

            return Response(player, status=status.HTTP_200_OK)
        else:
            return Response(
                {
                    'error_message': 'There was a problem reading player data'
                },
                status=player_response['status_code'])


class GetPlayerByName(APIView):
    def post(self, request, format=None):

        parser_classes = (JSONParser, )
        data = request.data
        client = GenericClient(data['oauth_token'])
        resp = client.read_player_data({'player_name': data['player_name']})

        if (resp['status_code'] == status.HTTP_200_OK):
            player_data = resp['response']
            player = {
                'error_message': player_data.error_message,
                'player_name': player_data.player_name,
                'avatar': player_data.avatar,
            }
            return Response(player, status=status.HTTP_200_OK)
        else:
            return Response({
                'error_message': 'Could not find player'
            },
                            status=resp['status_code'])


class GetPlayerPayments(APIView):
    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):
        try:
            obj = Player.objects.get(
                player_name=request.data['player_name'],
                email=request.data['email'])
            data_ready_for_json = list(
                PlayerPayment.objects.all().filter(player_id=obj.id).values(
                    'player_id', 'amount', 'stripe_response', 'id'))
            return Response({"success": True, "content": data_ready_for_json})
        except Player.DoesNotExist:
            return Response({"success": False})


class GetPlayerReferrals(APIView):
    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):
        try:

            players = Player.objects.filter(
                referrer_name=request.data['player_name'], )

            serialize = PlayerSerializer(players, many=True)
            return HttpResponse(json.dumps(serialize.data))
        except Player.DoesNotExist:
            return Response({"success": False})


class SavePlayerContacts(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        player_name = request.data['player_name']
        contacts = json.loads(request.data['contacts'])
        contact_type = request.data['contact_type']

        save_player_contacts(player_name, contacts, contact_type)

        return Response({"success": True})


def save_player_contacts(player_name, contacts, contact_type):

    obj = Player.objects.get(player_name=player_name, )
    dict_obj = model_to_dict(obj)

    for item in contacts:

        try:
            contactName = item['name'].encode('utf-8', 'ignore').decode(
                'utf-8', 'ignore').encode('utf-8')

            PlayerContact.objects.update_or_create(
                player_id=dict_obj['id'],
                contact_id=item['contact_id'],
                contact_type=contact_type,
                defaults={
                    'name': contactName,
                    'deleted': 0,
                })
        except:
            PlayerContact.objects.update_or_create(
                player_id=dict_obj['id'],
                contact_id=item['contact_id'],
                contact_type=contact_type,
                defaults={
                    'name': item['name'].encode('ascii', 'replace'),
                    'deleted': 0,
                })


class SetContactAsDeleted(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        try:
            obj = Player.objects.get(player_name=request.data['player_name'])
            PlayerContact.objects.update_or_create(
                player_id=obj.id,
                contact_type=request.data['contact_type'],
                contact_id=request.data['contact_id'],
                defaults={
                    'deleted': 1,
                })
        except:
            return Response({"success": False})

        try:
            if request.data['contact_type'] == 'google':
                _playerB = Player.objects.get(email=request.data['contact_id'])
            elif request.data['contact_type'] == 'facebook':
                _playerB = Player.objects.get(
                    facebook=request.data['contact_id'])
            elif request.data['contact_type'] == 'twitter':
                _playerB = Player.objects.get(
                    twitter=request.data['contact_id'])
            elif request.data['contact_type'] == 'steam':
                _playerB = Player.objects.get(steam=request.data['contact_id'])
            elif request.data['contact_type'] == 'twitch':
                _playerB = Player.objects.get(
                    twitch=request.data['contact_id'])
        except:
            return Response({"success": True})

        PlayerContact.objects.filter(
            player_id=obj.id,
            contact_type='facebook',
            contact_id=_playerB.facebook).update(deleted=1)
        PlayerContact.objects.filter(
            player_id=obj.id, contact_type='google',
            contact_id=_playerB.email).update(deleted=1)
        PlayerContact.objects.filter(
            player_id=obj.id,
            contact_type='twitter',
            contact_id=_playerB.twitter).update(deleted=1)
        PlayerContact.objects.filter(
            player_id=obj.id, contact_type='steam',
            contact_id=_playerB.steam).update(deleted=1)
        PlayerContact.objects.filter(
            player_id=obj.id,
            contact_type='twitch',
            contact_id=_playerB.twitch).update(deleted=1)

        return Response({"success": True})


class SavePlayerFriendInvite(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        data = request.data

        if Player.objects.filter(email=data['friend_email']).exists():
            player = Player.objects.get(email=data['friend_email'])
            message = 'Your friend ' + data[
                'friend_name'] + ' is already playing Fractured Veil!'
            return Response({
                "success": message,
                "player_name": player.player_name
            })

        player, created = PlayerFriendInvite.objects.update_or_create(
            player_name=data['player_name'],
            friend_email=data['friend_email'],
            defaults={
                'player_name': data['player_name'],
                'friend_name': data['friend_name'],
                'friend_email': data['friend_email'],
                'friend_message': data['friend_message'],
            })
        message = 'Invite has been sent!'

        mail_helper.send_message(
            'Your friend wants you to join him in the veil!',
            data['friend_message'],
            data['friend_email'],
            data['friend_name'],
            data['player_name'],
            'no-reply@fracturedveil.com',
            'friend_invite.html',
        )

        return Response({"success": message})


class GetPlayerFriendInvites(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        data = request.data
        data_ready_for_json = list(PlayerFriendInvite.objects.all().filter(
            player_name=data['player_name'], ).values(
                'friend_email',
                'friend_name',
                'friend_status',
                'created',
                'id',
            ))

        return Response({
            "success": True,
            'invites': data_ready_for_json
        },
                        status=status.HTTP_200_OK)


class CancelFriendInvite(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        data = request.data
        try:
            invite = PlayerFriendInvite.objects.get(id=data['invite_id'])
            invite.delete()
            return Response({"success": True}, status=status.HTTP_200_OK)
        except PlayerFriendInvite.DoesNotExist:
            return Response({"success": True}, status=status.HTTP_200_OK)


class RejectFriendInvite(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        data = request.data
        try:
            invite = PlayerFriendInvite.objects.get(
                player_name=data['player_name'],
                friend_email=data['friend_email'],
            )
            invite.friend_status = 'Rejected'
            invite.save()
            return Response({"success": True}, status=status.HTTP_200_OK)
        except PlayerFriendInvite.DoesNotExist:
            return Response({"success": True}, status=status.HTTP_200_OK)


class GetPlayerContacts(APIView):

    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        contact_type = request.data['contact_type']
        is_registered = request.data['is_registered']
        is_deleted = request.data['is_deleted']
        selected_user = []

        try:
            obj = Player.objects.get(player_name=request.data['player_name'])
        except Player.DoesNotExist:
            return ""

        if contact_type == '0':
            contacts = PlayerContact.objects.all().filter(
                player_id=obj.id, deleted=is_deleted).values(
                    'contact_id', 'contact_type', 'name', 'deleted')
        else:
            contacts = PlayerContact.objects.all().filter(
                player_id=obj.id,
                deleted=is_deleted).filter(contact_type=contact_type).values(
                    'contact_id', 'contact_type', 'name', 'deleted')

        for contact in contacts:
            _player = []
            if contact_type == 'google':
                _player = Player.objects.all().filter(
                    email=contact['contact_id'])
            elif contact_type == 'facebook':
                _player = Player.objects.all().filter(
                    facebook=contact['contact_id'])
            elif contact_type == 'twitter':
                _player = Player.objects.all().filter(
                    twitter=contact['contact_id'])
            elif contact_type == 'steam':
                _player = Player.objects.all().filter(
                    steam=contact['contact_id'])
            elif contact_type == 'twitch':
                _player = Player.objects.all().filter(
                    twitch=contact['contact_id'])
            elif contact_type == '0':
                _player = Player.objects.all().filter(
                    email=contact['contact_id']) | Player.objects.all().filter(
                        facebook=contact['contact_id']
                    ) | Player.objects.all().filter(
                        twitter=contact['contact_id']) | Player.objects.all(
                        ).filter(
                            twitch=contact['contact_id']) | Player.objects.all(
                            ).filter(steam=contact['contact_id'])

            contact['is_deleted'] = int(contact['deleted'])
            contact.pop('deleted')
            contact['contact_name'] = contact['name']
            contact.pop('name')

            if _player:
                contact['player_name'] = _player[0].player_name
                contact['is_registered'] = 1
            else:
                contact['is_registered'] = 0

            if (_player and is_registered == '1') or (
                    not _player and is_registered == '0'
                    and contact_type == '0') or (
                        not _player and is_registered == '0'
                        and contact_type == contact['contact_type']):
                selected_user.append(contact)

        if is_registered != "2":
            contacts = selected_user

        data_ready_for_json = list(contacts)

        return Response({"success": True, "content": data_ready_for_json})


class ProcessPayment(APIView):
    parser_classes = (JSONParser, )
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, format=None):

        package = Package.objects.get(pk=request.data['package_id'])
        player = create_update_player(request.data, package.id)

        try:

            price = int(float(package.price)) * 100
            package_name = package.title

            if (settings.IS_PRODUCTION):
                stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
            else:
                stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

            description = "Fractured Veil Create Player - Package: "
            description = description + package_name

            try:
                charge = stripe.Charge.create(
                    amount=price,
                    currency="usd",
                    source=request.data['token'],
                    receipt_email=request.data['email'],
                    description=description,
                    metadata={
                        'package': package_name,
                        'player_name': request.data['player_name'],
                        'price': price,
                        'email': request.data['email'],
                        'real_name': request.data['real_name'],
                    })
            except stripe.error.CardError, e:
                body = e.json_body
                err = body['error']
                return Response({
                    'status': 'error',
                    'type': err.get('type', None),
                    'code': err.get('code', None),
                    'param': err.get('param', None),
                    'message': err.get('message', None),
                })

            if (charge['status'] == 'succeeded'):
                process_payment(player.id, charge, price,
                                request.data['oauth_token'])
                return Response({"status": "success"})
            else:
                return Response({"success": False})
        except stripe.error.CardError as e:
            pass
        return Response({"success": True})


def create_update_player(player_data, package_id):

    player, created = Player.objects.update_or_create(
        email=player_data['email'],
        defaults={
            'player_name': player_data['player_name'],
            'package_id': package_id,
            'email': player_data['email'],
            'real_name': player_data['real_name'],
        })
    return player


def process_payment(player_id, stripe_response, amount, oauth_token):
    payment = PlayerPayment(
        player_id=player_id,
        stripe_response=stripe_response,
        amount=amount,
    )
    payment.save()
    post_payment_process(payment.pk, oauth_token)


def post_payment_process(payment_id, oauth_token):

    payment = PlayerPayment.objects.get(pk=payment_id)
    player = Player.objects.get(pk=payment.player_id)
    stripe_response = json.loads(payment.stripe_response)

    data = {
        'package_name': player.package.title,
        'amount_charged_cents': stripe_response['amount'],
        'stripe_transaction_id': stripe_response['id'],
    }

    client = GenericClient(oauth_token)
    resp = client.notify_package_purchased(data)

    if (resp['status_code'] == status.HTTP_200_OK):

        if (settings.IS_PRODUCTION):
            stripe.api_key = settings.STRIPE_LIVE_SECRET_KEY
        else:
            stripe.api_key = settings.STRIPE_TEST_SECRET_KEY

        refund = stripe.Refund.create(charge=stripe_response['id'])
        player_refund = PlayerPayment(
            stripe_response=refund,
            player_id=player.pk,
            amount=stripe_response['amount'],
        )
        player_refund.save()

    return status.HTTP_200_OK
